# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | |
| jak se mi to podařilo rozplánovat | |
| design zapojení | *URL obrázku designu* |
| proč jsem zvolil tento design | |
| zapojení | *URL obrázku zapojení* |
| z jakých součástí se zapojení skládá | |
| realizace | *URL obrázku hotového produktu* |
| nápad, v jakém produktu vše propojit dohromady|  |
| co se mi povedlo | |
| co se mi nepovedlo/příště bych udělal/a jinak | |
| zhodnocení celé tvorby | |